#! python

"""
PythonRun
Run in Python. 

"""

# Import modules. 
import os, subprocess, sys

# Configure options here! 
configuration = {}

# Create user data. 
user = {}

# Prevent loading if imported. Use os or subprocess instead! 
if "__main__" == __name__: 
	# Functions. 
	def startup(): 
		"""
			code to run in startup
		"""
		
		try: 
			user['Python Runtime'] = {'version': sys.version_info}
			print("PythonRun")
		except: 
			exit()
	
	def userinput(): 
		"""
			Display prompt and get user input. 
			
			Returns: user input
		"""
		
		if 'ready prompt' in configuration.keys(): 
			if not('\n' in configuration['ready prompt'][-2:]) and len(configuration['ready prompt']) >= 3: 
				configuration['ready prompt'] = configuration['ready prompt'] + '\n'
			elif not(' ' in configuration['ready prompt'][-1:]) and not('\n' in configuration['ready prompt'][-2:]): 
				configuration['ready prompt'] = configuration['ready prompt'] + ' '
		else: 
			configuration['ready prompt'] = "Ready.\n"
				
		
		while True: 
			try: 
				if user['Python Runtime']['version'][0] >= 3: 
					user_input = input(configuration['ready prompt'])
				else: 
					user_input = raw_input(configuration['ready prompt'])
			
				if user_input: 
					# Only allow non-empty
					return(user_input.strip())
			
			except KeyboardInterrupt: 
				pass
	
	def run(command: str): 
		"""
			Run the code. 
			
			Parameters: 
				command (str): the command to the run
			Returns: the runtime result
		"""
		
		try: 
			if 'run option' in configuration.keys(): 
				if 'os' in configuration['run command']: 
					result = os.system(command)
				elif 'subprocess' in configuration['run command']: 
					result = subprocess.call(command)
			else: 
				result = os.system(command)
		
		except KeyboardInterrupt: 
			print("Cancelled")
			return(False)
	
		else: 
			print(result)
			return(result)
	
	def main(): 
		startup()
		
		while True: 
			user['command'] = userinput()
			
			if user['command'] == 'exit' or user['command'] == 'quit': 
				return (None)
			else: 
				run(user['command'])
	
	main()
		
		
